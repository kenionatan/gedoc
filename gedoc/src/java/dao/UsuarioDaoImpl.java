package dao;
import java.util.List;
import model.Usuario;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class UsuarioDaoImpl implements UsuarioDao {

    @Override
    public Usuario findByUsuario(Usuario usuario) {
        Usuario model = null;
        final Session session = HibernateUtil.getSession();
        String sql = "FROM Usuario WHERE usuario = '"+usuario.getUsuario()+"'";
        try{
            final Transaction trans = session.beginTransaction();
            try{
            //session.beginTransaction();
            model = (Usuario) session.createQuery(sql).uniqueResult();
            trans.commit();
            
            
            
            }catch (HibernateException e){
                trans.rollback();
                throw e;
            
            }
        
        }finally{
            HibernateUtil.closeSession();
        }
        return model;
    }


    @Override
    public Usuario login(Usuario usuario) {
        Usuario model = this.findByUsuario(usuario);
        if(model != null){
            if(!usuario.getSenha().equals(model.getSenha())){
                model = null;
            }
        }
        return model;
    }

    @Override
    public List<Usuario> findAll() {
        List<Usuario> listado = null;
        final Session session = HibernateUtil.getSession();
        String sql = "FROM Usuario";
        try {
            final Transaction trans = session.beginTransaction();
            try{
            //session.beginTransaction();
            listado = session.createQuery(sql).list();
            trans.commit();
        } catch (HibernateException ex) {
            trans.rollback();
                throw ex;
        }
        }finally{
            HibernateUtil.closeSession();
        }
        return listado;
        
    }
    
    
}

