package beans;

import dao.UsuarioDao;
import dao.UsuarioDaoImpl;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import model.Usuario;
import org.primefaces.context.RequestContext;
import util.MyUtil;

@Named(value="loginBean")
@SessionScoped
public class loginBean implements Serializable{
    
    private Usuario usuario;
    private UsuarioDao usuarioDao;
    
    public loginBean() {
        this.usuarioDao = new UsuarioDaoImpl();
        if(this.usuario == null){
            this.usuario = new Usuario();
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

        public void login(ActionEvent actionEvent) {
                RequestContext context = RequestContext.getCurrentInstance();
                String rota = "";
                FacesMessage msg;
                boolean loggedIn;
                this.usuario = this.usuarioDao.login(this.usuario);
                if(this.usuario != null) {
                        loggedIn = true;
                        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", this.usuario.getUsuario());
                        msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bem-vindo", this.usuario.getUsuario());
                        rota = MyUtil.basepathlogin()+"views/index.xhtml";
                } else {
                        loggedIn = false;
                        msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro ao Logar", "Credenciais Inválidas");
                        if(this.usuario == null){
                            this.usuario = new Usuario();
                        }
                }
                
                FacesContext.getCurrentInstance().addMessage(null, msg);
                context.addCallbackParam("loggedIn", loggedIn);
                context.addCallbackParam("rota", rota);
        }
        public void logout(){
            String rota = MyUtil.basepathlogin()+"login.xhtml";
            RequestContext context = RequestContext.getCurrentInstance();
            FacesContext facesContext = FacesContext.getCurrentInstance();
            
            HttpSession sesion = (HttpSession) facesContext.getExternalContext().getSession(false);
            sesion.invalidate();
            
            context.addCallbackParam("loggetOut", true);
            context.addCallbackParam("rota", rota);
        }
    
}
