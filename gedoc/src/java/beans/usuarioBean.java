
package beans;

import dao.UsuarioDao;
import dao.UsuarioDaoImpl;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import model.Usuario;


@Named(value = "usuarioBean")
@RequestScoped
public class usuarioBean {

    
    private List<Usuario> usuarios;
    private Usuario selectedUsuario;
    
    public usuarioBean() {
        this.usuarios = new ArrayList<Usuario>();
    }

    public Usuario getSelectedUsuario() {
        return selectedUsuario;
    }
    


    public void setSelectedUsuario(Usuario selectedUsuario) {
        this.selectedUsuario = selectedUsuario;
    }
    
    public List<Usuario> getUsuarios(){
        UsuarioDao usuarioDao = new UsuarioDaoImpl();
        this.usuarios = usuarioDao.findAll();
        return usuarios;
    }
    
    
    
}
